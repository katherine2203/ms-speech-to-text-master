const express = require('express');
const uuidv5 = require('uuid/v5');
const fs = require('fs');
const multer  = require('multer');
const upload = multer({ dest: 'audios/' });

const router = express.Router();

router.post('/recognize', upload.single('audio'), function(req, res) {
  const speechToText = req.speechToText;
  const path = req.file.path;
  const params = {
    audio: fs.createReadStream(path),
    contentType: 'audio/webm',
    model: "es-PE_BroadbandModel"
  };  

  speechToText.recognize(params)
  .then(response => {
    return res.json(response.result);
  })
  .catch(err => {
    return res.json({
      message: err
    });
  });
});

module.exports = router;