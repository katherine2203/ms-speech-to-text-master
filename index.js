const express = require('express');
const cfenv = require("cfenv");
const dotenv = require('dotenv');
const cors = require("cors");

const app = express();
const appEnv = cfenv.getAppEnv();

app.use(cors());

dotenv.config();

app.use(require('./middleware'));
app.use('/', require('./routes'));

app.listen(appEnv.port, appEnv.bind, function() {
  console.log("server starting on " + appEnv.url);
});