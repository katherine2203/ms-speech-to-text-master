"use strict";

const SpeechToTextV1 = require('ibm-watson/speech-to-text/v1');
const { IamAuthenticator } = require('ibm-watson/auth');

module.exports = function(req, res, next) {
  const speechToText = new SpeechToTextV1({
    authenticator: new IamAuthenticator({ 
      apikey: process.env.APIKEY
    }),
    url: process.env.URL
  });
  req.speechToText = speechToText;
  next();
};